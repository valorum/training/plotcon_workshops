# Getting Started with Python for Data Analysis - Boston April 2018

Syllabus for Boston Python Workshop, April 2018.

Our workshop assumes no prior programming experience and ensures that each of our
attendees is capable of cleaning, merging, analyzing, and visualizing data using
the Python programming language.

The workshop is split into two days: day one covering Python foundations and
day two tackling data analysis and visualization.

## Day 1: Python foundations

In this section we start with installation and work our way through core Python
concepts and skills.

**Outline of topics**:

- Introduction and installation
- First steps:
- Core data-types:
    - Strings
    - Numbers
    - Collections (lists and tuples)
    - Dictionaries
- Conditional statements
- Iteration
- Functions

## Day 2: Data analysis and visualization

This section of the workshop covers data ingestion, cleaning, manipulation,
analysis, and visualization in Python

We build on the skills learned in the Python foundations section and learn how
to use the [pandas](https://pandas.pydata.org) library for handling data and
introduce the [plotly](https://plot.ly.python) library for data visualization

**Outline of topics**:

- Introduction to pandas
- Basic functionality
- The index: labeling rows and columns of the data
- Data cleaning and importing messy data
- Reshaping data
- Merging datasets
- Performing analysis on groups
- Time series analysis
- Intermediate plotting and visualization
