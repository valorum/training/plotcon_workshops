# Getting Started with Python for Data Analysis - Boston April 2018

This course is taught by Chase Coleman and Spencer Lyon. They are PhD students
at NYU's Stern School of Business and co-founded Valorum Data, a data consulting
and training company.

Our workshop assumes no prior programming experience and ensures that each of
our attendees is capable of cleaning, merging, analyzing, and visualizing data
using the Python programming language.

Upon completion of the workshop, you have the tools to not only replicate and
improve your current data analysis workflow, but that you also will be able to
leverage Python to perform new types of analysis, automate redundant data
tasks, and benefit from access to the Python ecosystem and community.

Please check out the [syllabus](https://gitlab.com/valorum/training/plotcon_workshops/blob/master/syllabus.md) for more details.
